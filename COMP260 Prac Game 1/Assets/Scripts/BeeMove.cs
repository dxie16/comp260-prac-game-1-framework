﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
    // Use this for initialization
    private float speed;        // metres per second
    private float turnSpeed;  // degrees per second
    public Transform target;
    public Transform target2;
    private Vector2 heading = Vector2.right;
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    public ParticleSystem explosionPrefab;
    void Start () {
        
        // find a player object to be the target by type
        // Note: this is not standard Unity syntax
        PlayerMove player = FindObjectOfType<PlayerMove>();
        target = player.transform;
        PlayerMoveP2 P2 = (PlayerMoveP2)FindObjectOfType(typeof(PlayerMoveP2));
        target2 = P2.transform;
        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
    }

    // Update is called once per frame
    void Update () {
        
        // get the vector from the bee to the target 
        Vector2 direction = target.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;

        if (direction.magnitude > direction2.magnitude)
        {
            Vector2 temp = direction;
            direction = direction2;
            direction2 = temp;
        }

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        Vector2 velocity = direction * speed;
        transform.Translate(heading * speed * Time.deltaTime);

    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);
    }
}
