﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
    public Vector2 move;
    public Vector2 velocity; // in metres per second
    public string HorizontalAxis;
    public string VerticalAxis;
    public float maxSpeed = 100.0f; // in metres per second
    public float acceleration = 10.0f; // in metres/second/second
    public float brake = 100.0f; // in metres/second/second
    private float speed = 0.0f;    // in metres/second
    public float turnSpeed = 30.0f; // in degrees/second
    private BeeSpawner beeSpawner;
    public float destroyRadius = 1.0f;
    // Use this for initialization
    void Start () {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }
	
	// Update is called once per frame
	void Update () {
        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(VerticalAxis);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
            }
            else if (speed > 0)
            {
                speed = speed + brake * Time.deltaTime;
            }
            else
            {
                speed = 0;
            }
        }

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;

        // the horizontal axis controls the turn
        float turn = Input.GetAxis(HorizontalAxis);

        // turn the car
        transform.Rotate(0, 0, turn * -turnSpeed * speed * Time.deltaTime);
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }
    }
}
